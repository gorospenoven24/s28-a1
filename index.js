//  num 3
fetch('https://jsonplaceholder.typicode.com/todos/',{
	method: 'GET',
	
	
}).then((response) => response.json()).then((json) => console.log(json));

//  num 4 
// displaying only the title of the json file
fetch('https://jsonplaceholder.typicode.com/todos').then((response) => response.json()).then((json) => console.log(json.map(items => items.title)));


// Num 5

fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'GET',
}).then((response) => response.json()).then((json) => console.log(json));
 
 // num 6

fetch('https://jsonplaceholder.typicode.com/todos/1').then((response) => response.json()).then((json) => console.log(`the item "${json.title}" on the list has a status of ${json.completed}.`));



// num 7

fetch('https://jsonplaceholder.typicode.com/todos/',{
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		id: 201,
		title : "created To do list Item",
		userId: 1
	})
}).then((response) => response.json()).then((json) => console.log(json));


// number 8 
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted : 'Pending',
		description: 'To update the mmy to do list with a different data structure',
		id: 1,
		status: 'pending',
		title:'updated to do list item',
		userId: 1
	})

}).then((response) => response.json()).then((json) => console.log(json));

// number 9

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'Application/json'
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		id: 1,
		status: "Pending",
		title: 'Updated To Do List Item',
		userId: 1
	})
	}).then((response) => response.json()).then((json) => console.log(json));


// num 10

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'Application/json'
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: '07/09/21',
		status: "Complete",
		title: 'delectus aut autem',
		userId: 1
	})
}).then((response) => response.json()).then((json) => console.log(json));


// num 11
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'Application/json'
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: '07/09/21',
		status: "Complete",
		title: 'delectus aut autem',
		userId: 1
	})
}).then((response) => response.json()).then((json) => console.log(json));


// num 13
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'DELETE'
});